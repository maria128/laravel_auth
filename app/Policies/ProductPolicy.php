<?php

namespace App\Policies;

use App\Entity\User;
use App\Entity\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create products.
     *
     * @param  \App\Entity\User  $user
     * @return mixed
     */
    public function edit(User $user, Product $product)
    {
        return $user->getId() == $product->getUserId() || $user->isAdmin();
    }

    /**
     * Determine whether the user can update the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function update(User $user, Product $product)
    {
        return $user->getId() == $product->getUserId() || $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function destroy(User $user, Product $product)
    {
        return $user->getId() == $product->getUserId() || $user->isAdmin();
    }

}
