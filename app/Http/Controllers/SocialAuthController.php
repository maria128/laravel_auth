<?php
namespace App\Http\Controllers;
use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    protected $redirectPath = '/products';

    public function redirectToProvider(Request $request) {

        return Socialite::driver('facebook')->redirect();
    }
    public function handleProviderCallback()
    {

        $socialUser = Socialite::driver('facebook')->user();

        $user = User::where(['email' => $socialUser->getEmail()])->first();

        if (is_null($user)) {
            $user = User::create([
                'name' => $socialUser->getName(),
                'email' => $socialUser->getEmail(),
            ]);
        }
        Auth::login($user);

        return redirect($this->redirectPath);
    }
}