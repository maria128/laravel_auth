@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div>
                    <a style="margin: 19px;" href="{{ route('products.create')}}" class="btn btn-primary">New
                        product</a>
                </div>
            </div>

            <div class="col-sm-12">
                <h1 class="display-3">Products</h1>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Price</td>
                        <td colspan=3>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}} </td>
                            <td>{{$product->price}}</td>


                                <td>
                                    @can('edit', $product)
                                    <a href="{{ route('products.edit',$product->id)}}" class="btn btn-primary">Edit</a>
                                    @endcan
                                </td>


                            <td>
                                <a href="{{ route('products.show',$product->id)}}" class="btn btn-warning">Show</a>
                            </td>

                                <td>
                                    @can('destroy', $product)
                                    <form action="{{ route('products.destroy', $product->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                    @endcan
                                </td>


                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div>
                </div>
            </div>
@endsection